# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_10_124815) do

	# These are extensions that must be enabled in order to support this database
	enable_extension "plpgsql"

	create_table "group_likes", force: :cascade do |t|
		t.bigint "user_id"
		t.bigint "group_id"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.boolean "like"
		t.index ["group_id"], name: "index_group_likes_on_group_id"
		t.index ["user_id"], name: "index_group_likes_on_user_id"
	end

	create_table "groups", force: :cascade do |t|
		t.string "key"
		t.string "display_name"
		t.bigint "user_id"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.string "read_me"
		t.bigint "download"
		t.index ["user_id"], name: "index_groups_on_user_id"
	end

	create_table "template_group_pairs", force: :cascade do |t|
		t.bigint "template_id"
		t.bigint "group_id"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.index ["group_id"], name: "index_template_group_pairs_on_group_id"
		t.index ["template_id"], name: "index_template_group_pairs_on_template_id"
	end

	create_table "template_likes", force: :cascade do |t|
		t.bigint "user_id"
		t.bigint "template_id"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.boolean "like"
		t.index ["template_id"], name: "index_template_likes_on_template_id"
		t.index ["user_id"], name: "index_template_likes_on_user_id"
	end

	create_table "templates", force: :cascade do |t|
		t.string "git_link"
		t.string "key"
		t.string "display_name"
		t.bigint "user_id"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.string "read_me"
		t.bigint "download"
		t.index ["user_id"], name: "index_templates_on_user_id"
	end

	create_table "tokens", force: :cascade do |t|
		t.string "secret"
		t.string "key"
		t.date "expiry"
		t.bigint "user_id"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
		t.index ["user_id"], name: "index_tokens_on_user_id"
	end

	create_table "users", force: :cascade do |t|
		t.string "email"
		t.datetime "created_at", null: false
		t.datetime "updated_at", null: false
	end

	add_foreign_key "group_likes", "groups"
	add_foreign_key "group_likes", "users"
	add_foreign_key "groups", "users"
	add_foreign_key "template_group_pairs", "groups"
	add_foreign_key "template_group_pairs", "templates"
	add_foreign_key "template_likes", "templates"
	add_foreign_key "template_likes", "users"
	add_foreign_key "templates", "users"
	add_foreign_key "tokens", "users"
end
