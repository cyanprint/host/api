class AddReadMeToTemplate < ActiveRecord::Migration[5.2]
	def change
		add_column :templates, :read_me, :string
	end
end
