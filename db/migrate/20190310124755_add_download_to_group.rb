class AddDownloadToGroup < ActiveRecord::Migration[5.2]
	def change
		add_column :groups, :download, :bigint
	end
end
