class CreateTemplates < ActiveRecord::Migration[5.2]
	def change
		create_table :templates do |t|
			t.string :git_link
			t.string :key
			t.string :display_name
			t.references :user, foreign_key: true

			t.timestamps
		end
	end
end
