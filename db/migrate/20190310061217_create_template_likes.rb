class CreateTemplateLikes < ActiveRecord::Migration[5.2]
	def change
		create_table :template_likes do |t|
			t.references :user, foreign_key: true
			t.references :template, foreign_key: true

			t.timestamps
		end
	end
end
