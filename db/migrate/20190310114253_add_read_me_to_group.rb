class AddReadMeToGroup < ActiveRecord::Migration[5.2]
	def change
		add_column :groups, :read_me, :string
	end
end
