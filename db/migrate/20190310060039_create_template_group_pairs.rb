class CreateTemplateGroupPairs < ActiveRecord::Migration[5.2]
	def change
		create_table :template_group_pairs do |t|
			t.references :template, foreign_key: true
			t.references :group, foreign_key: true

			t.timestamps
		end
	end
end
