class AddLikeToGroupLike < ActiveRecord::Migration[5.2]
	def change
		add_column :group_likes, :like, :boolean
	end
end
