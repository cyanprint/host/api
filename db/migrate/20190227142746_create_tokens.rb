class CreateTokens < ActiveRecord::Migration[5.2]
	def change
		create_table :tokens do |t|
			t.string :secret
			t.string :key
			t.date :expiry
			t.references :user, foreign_key: true

			t.timestamps
		end
	end
end
