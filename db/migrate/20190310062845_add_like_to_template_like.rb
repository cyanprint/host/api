class AddLikeToTemplateLike < ActiveRecord::Migration[5.2]
	def change
		add_column :template_likes, :like, :boolean
	end
end
