require 'singleton'
require 'bucket_client'

class BlobService
	include Singleton

	def initialize
		if Rails.env == "production"
			@client = BucketClient::generate type: :aws, id: ENV["AWS_ID"], secret: ENV["AWS_SECRET"], region: ENV["AWS_REGION"]
		else
			@client = BucketClient::generate type: :local, path: "public/bucket-host", principal: "bucket-host"
		end

	end

	# @return [BucketClient::Client]
	def client
		@client
	end

end