class TokenService

	def get_all(email)
		user = User.includes(:tokens).find_by!(email: email)
		user.tokens.map(&method(:to_view_model))
	end

	# @param [String] email
	# @param [String] key
	# @param [Integer] days
	def create(email, key, days)
		raise BadRequest.new("Days have to be large than 1") if days < 1
		user = User.find_by!(email: email)
		expiry = Date.current + days
		secret = SecureRandom.hex 32
		token = Token.create!(key: key, secret: secret, expiry: expiry, user_id: user.id)
		{
			id: token.id,
			key: token.key,
			expiry: token.expiry,
			secret: token.secret,
		}
	end


	def delete(email, id)
		p id
		p Token.includes(:user)
		# @type [Token]
		token = Token.includes(:user).find_by!(id: id)
		raise AuthorizationFailed.new("Not owner") unless token.user.email == email
		token.destroy!
	end


	private

		def to_view_model(token)
			{
				id: token.id,
				key: token.key,
				expiry: token.expiry
			}
		end

end