require "base64"
require "blob_service"
require "kirinnee_core"

class CRUDService
	def initialize
		@client = BlobService.instance.client
	end

	def upload_readme(base64, folder, name)
		bucket = @client.put_bucket! "cyanprint"
		payload = Base64.decode64 base64
		name = name.replace_all(" ", "_").replace_all("-", "_").downcase
		raise BadRequest.new("File size cannot exceed 1MB") if payload.length > 1000000
		bucket.put_blob!(payload, "readme/#{folder}/#{name}.md")
	end

end