class GroupService < CRUDService


	# @param [String] key
	def bump_download(key)
		group = Group.find_by!(key: key)
		new_count = group.download + 1
		group.update! download: new_count
	end

	# @param [String] name
	# @param [String] key
	# @param [String] secret
	# @param [String] email
	# @param [Array<String>] templates
	def put(name, key, secret, email, readme, templates)
		exist = Group.exists? key: key
		if exist
			update name, key, secret, email, readme, templates
		else
			create name, key, secret, email, readme, templates
		end
	end

	# @param [String] name
	# @param [String] key
	# @param [String] secret
	# @param [String] email
	# @param [Array<String>] templates
	def create(name, key, secret, email, readme, templates)
		# Get pointer to user
		user = User.includes(:tokens).find_by!(email: email)
		# check if token is owned by user
		token = user.tokens.find_by!(secret: secret)
		# raise exception if token is expired
		raise BadRequest.new("Token Expired") if token.expiry < Date.current
		# validate that the templates do not contain repeats
		raise BadRequest.new("Repeated templates") unless templates.uniq == templates
		# verify that templates exist
		temps = Template.where(key: templates)
		raise NotFound.new("Template(s) not found") if temps.count != templates.length
		# upload readme
		uri = upload_readme readme, "group", name
		# create the group
		group = Group.create!(display_name: name, user_id: user.id, read_me: uri, key: key, download: 0)
		# create relations for each template
		temps.to_a.each do |x|
			TemplateGroupPair.create!(template_id: x.id, group_id: group.id)
		end
		# return view model
		to_view_model group
	end

	# @param [String] name
	# @param [String] key
	# @param [String] secret
	# @param [String] email
	# @param [Array<String>] templates
	def update(name, key, secret, email, readme, templates)
		# Get pointer to user
		user = User.includes(:tokens).find_by!(email: email)
		# @type [Group]
		group = Group.find_by!(key: key)
		# check if group is owned by user
		raise AuthorizationFailed.new("Not group owner") unless group.user.id == user.id
		# check if token is owned by user
		token = user.tokens.find_by!(secret: secret)
		# raise exception if token is expired
		raise BadRequest.new("Token Expired") if token.expiry < Date.current
		# validate that the templates do not contain repeats
		raise BadRequest.new("Repeated templates") unless templates.uniq == templates
		# verify that templates exist
		temps = Template.where(key: templates)
		raise NotFound.new("Template(s) not found") if temps.count != templates.length
		# delete all old relations
		group.template_group_pairs.destroy_all
		# update readme
		uri = upload_readme readme, "group", name
		# update group details
		group.update! display_name: name, read_me: uri
		# create relations for each template
		temps.to_a.each do |x|
			TemplateGroupPair.create!(template_id: x.id, group_id: group.id)
		end
		# return view model
		to_view_model group
	end

	def get_all(search = nil)
		groups = Group.includes(:user, :group_likes, :template_group_pairs)
		groups = groups.where("key ILIKE ? or display_name ILIKE ?", "%#{search}%", "%#{search}%")
		groups.map(&method(:to_view_model))
	end

	def by_key(key)
		group = Group.find_by!(key: key)
		to_view_model group
	end

	def like(key, email, like)
		user = User.find_by!(email: email)
		group = Group.find_by!(key: key)
		group_like = GroupLike.find_by(user_id: user.id, group_id: group.id)
		if group_like.nil?
			GroupLike.create!(user_id: user.id, group_id: group.id, like: like)
			true
		else
			group_like.update!(like: like)
			like
		end
	end

	def like?(key, email)
		user = User.find_by!(email: email)
		group = Group.find_by!(key: key)
		group_like = GroupLike.find_by(user_id: user.id, group_id: group.id)
		!group_like.nil? && group_like.like
	end

	def get_user_groups(email)
		user = User.includes(:groups).find_by!(email: email)
		user.groups.map(&method(:to_view_model))
	end

	def get_user_liked_groups(email)
		user = User.includes(:group_likes).find_by!(email: email)
		user.group_likes.filter(&:like).map {|x| to_view_model(x.group)}
	end


	private

	# @param [Group] group
	def to_view_model(group)
		templates = group.templates.select(:key).map(&:key)
		{
			display_name: group.display_name,
			unique_key: group.key,
			downloads: group.download,
			stars: group.group_likes.where(like: true).count,
			readme: group.read_me,
			templates: templates,
			author: group.user.email
		}
	end
end