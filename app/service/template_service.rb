class TemplateService < CRUDService

	# @param [String] key
	def bump_download(key)
		template = Template.find_by!(key: key)
		new_count = template.download + 1
		template.update! download: new_count
	end

	# @param [String] name
	# @param [String] key
	# @param [String] git_link
	# @param [String] secret
	# @param [String] email
	def put(name, key, git_link, secret, email, readme)
		exist = Template.exists? key: key
		if exist
			update name, key, git_link, secret, email, readme
		else
			create name, key, git_link, secret, email, readme
		end
	end


	def get_all(search = nil)
		templates = Template.includes(:user, :template_likes, :template_group_pairs)
		templates = templates.where("key ILIKE ? or display_name ILIKE ?", "%#{search}%", "%#{search}%") unless search.nil?
		templates.map(&method(:to_view_model))
	end

	def by_key(key)
		template = Template.find_by!(key: key)
		to_view_model template, true
	end

	def like(key, email, like)

		user = User.find_by!(email: email)
		template = Template.find_by!(key: key)
		# @type [TemplateLike]
		template_like = TemplateLike.find_by(user_id: user.id, template_id: template.id)
		if template_like.nil?
			TemplateLike.create!(user_id: user.id, template_id: template.id, like: like)
			true
		else
			template_like.update!(like: like)
			like
		end
	end

	def like?(key, email)
		user = User.find_by!(email: email)
		template = Template.find_by!(key: key)
		template_like = TemplateLike.find_by(user_id: user.id, template_id: template.id)
		!template_like.nil? && template_like.like
	end

	def get_user_templates(email)
		user = User.includes(:templates).find_by!(email: email)
		user.templates.map(&method(:to_view_model))
	end

	def get_user_liked_templates(email)
		user = User.includes(:template_likes).find_by!(email: email)
		user.template_likes.filter(&:like).map {|x| to_view_model(x.template, false)}
	end

	private

	# @return [User]
	def get_user(email)
		User.includes(:tokens).find_by!(email: email)
	end

	# @param [Template] template
	def to_view_model(template, include_group = false)
		ret = {
			display_name: template.display_name,
			unique_key: template.key,
			author: template.user.email,
			repository: template.git_link,
			downloads: template.download,
			stars: template.template_likes.where(like: true).count,
			readme: template.read_me
		}
		ret[:group] = template.groups.select(:key).map(&:key) if include_group
		ret
	end

	# @param [String] name
	# @param [String] key
	# @param [String] git_link
	# @param [String] secret
	# @param [String] email
	def update(name, key, git_link, secret, email, readme)
		# obtain pointer to user using email
		user = get_user email
		# obtain pointer to template using key
		template = Template.find_by!(key: key)
		# check if the template is owned by the user
		raise AuthorizationFailed.new("Not template owner") unless template.user.id == user.id
		# check if user owns token
		token = user.tokens.find_by!(secret: secret)
		# check for token expiry
		raise BadRequest.new("Token Expired") if token.expiry < Date.current
		# ALL OK

		# update read me
		uri = upload_readme readme, "template", name
		# updates template
		template.update!(git_link: git_link, display_name: name, read_me: uri)
		# return view model
		to_view_model template
	end

	# @param [String] name
	# @param [String] key
	# @param [String] git_link
	# @param [String] secret
	# @param [String] email
	def create(name, key, git_link, secret, email, readme)
		# obtain instance to user
		user = get_user email
		# check if owner owns the token
		token = user.tokens.find_by!(secret: secret)
		# check for token expiry
		raise BadRequest.new("Token Expired") if token.expiry < Date.current # check for token expiry
		# uploads readme to bucket
		uri = upload_readme readme, "template", name
		# create template
		template = Template.create!(git_link: git_link, display_name: name, user_id: user.id, read_me: uri, key: key, download: 0)
		# return view model
		to_view_model template
	end

end