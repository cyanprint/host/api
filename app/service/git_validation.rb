class GitValidation
	def is_git(link)
		begin
			URI::parse(link)
		rescue
			return false
		end
		system "git ls-remote \"#{link}\"", :out => File::NULL, :err => File::NULL
	end
end