class GroupLike < ApplicationRecord
	belongs_to :user
	belongs_to :group
	validates :like, inclusion: {in: [true, false]}
end
