class TemplateLike < ApplicationRecord
	belongs_to :user
	belongs_to :template
	validates :like, inclusion: {in: [true, false]}
end
