class Token < ApplicationRecord
	belongs_to :user
	validates :secret, presence: true, uniqueness: true,
	          format: {with: /\A[0-9a-f]{64}\z/}
	validates :expiry, presence: true
	validates :key, presence: true, format: {
		with: /\A[a-z][0-9a-z_\-]*\z/i,
		message: "contains illegal characters. Only alphanumeric, '_' and '-' allowed."
	}
end
