class Group < ApplicationRecord
	belongs_to :user
	has_many :template_group_pairs
	has_many :group_likes
	has_many :templates, through: :template_group_pairs

	validates :key, presence: true, uniqueness: true, format: {
		with: /\A[a-z][0-9a-z_]*\z/,
		message: "contains illegal characters. Only alphanumeric and '_' allowed."
	}
	validates :display_name, presence: true, format: {
		with: /\A[a-z][a-z0-9_\-\s]{2,}\z/i,
		message: "contains illegal characters. Only alphanumeric, '_', '-' amd spaces allowed."
	}
	validates :read_me, presence: true
	validates :download, presence: true
end
