class TemplateGroupPair < ApplicationRecord
	belongs_to :template
	belongs_to :group
end
