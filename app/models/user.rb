class User < ApplicationRecord
	validates :email, presence: true, format: {with: URI::MailTo::EMAIL_REGEXP}, uniqueness: true
	has_many :tokens
	has_many :templates
	has_many :groups
	has_many :group_likes
	has_many :template_likes
end
