class NotFound < StandardError

end

class BadRequest < StandardError

end

class AuthorizationFailed < StandardError

end