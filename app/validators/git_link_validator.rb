require "git_validation"

class GitLinkValidator < ActiveModel::EachValidator
	def validate_each(object, attribute, value)
		v = GitValidation.new
		unless v.is_git value
			object.errors[attribute] << "Not valid git link"
		end
	end
end