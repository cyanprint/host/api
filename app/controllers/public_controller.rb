require "template_service"
module Development
	def create_user
		email = params[:email]
		user = User.create!(email: email)
		json_response user
	end
end


module TemplateEndpoints
	# templates array
	def templates
		filter = params[:filter] || nil
		templates = @template_service.get_all filter
		json_response templates
	end

	# template view model
	def template_by_key
		from = request.headers[:cli]
		key = params[:key]
		@template_service.bump_download key if from == "kirinnee"
		template = @template_service.by_key key
		json_response template
	end

	# template view model
	def create_template
		template = @template_service.put params[:name], params[:key], params[:repo], params[:secret], params[:email], params[:readme]
		json_response template
	end
end

module GroupEndpoints
# return array of groups
	def groups
		filter = params[:filter] || nil
		groups = @group_service.get_all filter
		json_response groups
	end

	# return group view model
	def group_by_key
		from = request.headers[:cli]
		key = params[:key]
		@group_service.bump_download key if from == "kirinnee"
		group = @group_service.by_key key
		json_response group
	end

	# return group view model
	def create_group
		group = @group_service.put params[:name], params[:key], params[:secret], params[:email], params[:readme], params[:templates]
		json_response group
	end
end

class PublicController < ApplicationController
	include GroupEndpoints, TemplateEndpoints

	def users
		json_response User.all
	end
end

