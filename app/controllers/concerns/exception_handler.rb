require "custom_exception"
require "bucket_client/bucket_operation_exception"

module ExceptionHandler
	extend ActiveSupport::Concern

	included do

		rescue_from AuthorizationFailed do |e|
			json_response({message: e.message}, :unauthorized)
		end

		rescue_from ArgumentError do |e|
			json_response({message: e.message}, :bad_request)
		end

		rescue_from BucketOperationException do |e|
			json_response({message: e.message}, :bad_request)
		end

		rescue_from BadRequest do |e|
			json_response({message: e.message}, :bad_request)
		end

		rescue_from NotFound do |e|
			json_response({message: e.message}, :not_found)
		end

		rescue_from ActiveRecord::RecordNotFound do |e|
			json_response({message: e.message}, :not_found)
		end

		rescue_from ActiveRecord::RecordInvalid do |e|
			json_response({message: e.message}, :unprocessable_entity)
		end
	end
end