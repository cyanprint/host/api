class PrivateController < ApplicationController

	include Secured

	# no content
	def like_template
		like = @template_service.like(params[:key], get_user_email, params[:like])
		r = {like: like}
		json_response r
	end

	# no content
	def like_group
		like = @group_service.like(params[:key], get_user_email, params[:like])
		r = {like: like}
		json_response r
	end

	# return true or false
	def is_group_liked
		like = @group_service.like?(params[:key], get_user_email)
		r = {like: like}
		json_response r
	end

	# return true or flase
	def is_template_liked
		like = @template_service.like?(params[:key], get_user_email)
		r = {like: like}
		json_response r
	end

	# return all token view models
	def tokens
		tokens = @token_service.get_all get_user_email
		json_response tokens
	end

	# token view model, with secret
	def create_token
		token = @token_service.create get_user_email, params[:key], params[:expire_in]
		json_response token
	end

	# no content
	def delete_token
		@token_service.delete get_user_email, params[:id]
	end

	def templates
		templates = @template_service.get_user_templates get_user_email
		json_response templates
	end

	def liked_templates
		templates = @template_service.get_user_liked_templates get_user_email
		json_response templates
	end

	def groups
		groups = @group_service.get_user_groups get_user_email
		json_response groups
	end

	def liked_groups
		groups = @group_service.get_user_liked_groups get_user_email
		json_response groups
	end

	private

		def get_user_email
			email = @email
			User.create!(email: email) unless User.exists?(email: email)
			email
		end
end