require "kirinnee_core"
require "response"

class ApplicationController < ActionController::API
	include ExceptionHandler
	include JsonResponse

	def initialize
		@token_service = TokenService.new
		@template_service = TemplateService.new
		@group_service = GroupService.new
	end

end
