# CyanPrint API

CyanPrint to allow people to like and create templates and template groups

This API also allows for generation of API tokens, for them to publish templates to the CyanPrint repository


# Prerequisite

Everything (including test), runs with docker and docker-compose

- Docker
- Docker-compose
- Git

# Setting up Development and Test environment
Ensure you have the docker volume needed: `cyan-print-psql`

To list your volume: 
```bash
$ docker volume list
```

If the isn't any volume that matches, create the volume:
```bash
$ docker volume create cyan-print-psql
```

# Getting started

**All development can be done without ruby on your machine, all that is needed is docker and docker-compose**

**All commands (such as scaffolding, gem installation and migration) should be run within the docker container, 
instruction on how to access and setup the docker environment will be provided below**

After you have setup the environment, to start you need 2 more steps:

1. Create the development environment 
2. Access the development environment (open the terminal)

There are two ways to perform both steps, using docker directly or using a simple macro.

Macro requires ruby to be installed on your computer.

Please jump to [Using Marcos] if you want to use the macros
Please jump to [Using Docker] if you want to use docker directly

# Using Docker

### Development
To develop, you have to start a docker container as an development environment, and 
develop within it. 

Below shows two ways you can access the development terminal:

To start the development environment
```bash
$ docker-compose up --build -d
```

To access the development terminal, please attach to the running server instance:
```bash
$ docker exec -ti cyanprint_rails_1 bash
````

To stop the docker environment
```bash
$ docker-compose down
```

You can now run any command within the terminal

### Test
To run test:
```bash
$ docker-compose -f docker-compose.test.yml up --build --abort-on-container-exit
```

# Using Macros

Using macro needs a version of ruby installed.

### Development

To access the development terminal:
```bash
$ ruby s dev
```

To stop the development environment:
```bash
$ ruby s stop
```

### Test
To run test:
```bash
$ ruby s test
```

## Common commands to run within the development terminal

**After accessing the development terminal, within the attached docker instance**

Start Server:  `rails server`  
Rails Console: `rails console`  
Model Scaffolding:  `rails g model <name> <field>:<type>`  
Add Migration:  `rails g migration Add<FieldInPascal>To<ClassInPascal> <field_in_snake>:<type>`  
Remove Migration: `rails g migration Remove<FieldInPascal>From<ClassInPascal> <field_in_snake>:<type>`  
Create Database:  `rails db:create`  
Migrate Database:  `rails db:migrate`  
Rollback Database:  `rails db:rollback`    
Access Postgres: `psql -h postgres`  

For more: [Rails Cheatsheet](https://gist.github.com/mdang/95b4f54cadf12e7e0415)

# Production
1. Set relevant environment variables:
    - DB_PW
    - AWS_ID
    - AWS_SECRET
    - AWS_REGION
2. Ensure that the latest image is set as the environment variables:
    - IMAGE_NAME
3. Ensure docker-compose.production.yml is in target server-instance

4. Create named volume:
    ```bash
    $ docker volume create cyanprint
    ```
5. Start the docker-compose:
    ```bash
    $ docker-compose up -f docker-compose.production.yml
    ```