FROM ruby:2.6.0 as base
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

FROM base
COPY . .
RUN chmod o+x entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]