case ARGV[0]
	when "dev"
		r = system "docker exec -ti cyanprint_rails_instance bash"
		unless r
			system "docker-compose up --build -d"
			system "docker exec -ti cyanprint_rails_instance bash"
		end
	when "test"
		system "docker-compose -f docker-compose.test.yml up --build --abort-on-container-exit"
	when "stop"
		system "docker-compose down"
	else
		p "Unknown command. Possible commands: "
		p "	Start Development Terminal: ruby s dev"
		p "	Run test: ruby s test"
		p " Stop Development Environment: ruby s stop"

end