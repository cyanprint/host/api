require 'rails_helper'

describe User, type: :model do
	describe 'email field' do
		it {should validate_presence_of :email}
		it {should validate_uniqueness_of :email}
		it {should allow_values('kirinnee97@gmail.com', 'pluto.mars@yahoo.com', 'loco@chatinfinite').for :email}
		it {should_not allow_values('hey', 'abc@', '@gmail').for :email}
	end

	it {should have_many(:tokens)}
	it {should have_many(:templates)}
	it {should have_many(:groups)}
	it {should have_many(:group_likes)}
	it {should have_many(:template_likes)}
end
