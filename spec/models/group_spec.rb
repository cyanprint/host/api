require 'rails_helper'

describe Group, type: :model do

	describe "key" do
		it {should validate_presence_of :key}
		it {should validate_uniqueness_of :key}
		it {should allow_values("a", "azurekey", "azure_key").for(:key)}
		it {should_not allow_values("1", "azure*key", "azurekey!", "Azure-key", "Azure_key").for(:key)}
	end

	describe "display_name" do
		it {should validate_presence_of :display_name}
		it {should allow_values("Some Cool Name", "Very Cold Name", "under_score_case", "Dash-case", "with-1-2-numbers", "short").for(:display_name)}
		it {should_not allow_values("1-starts with number", "a!a!a!a!a", "comma,comma", "** actually", "aa").for(:display_name)}
	end
	describe "read_me" do
		it {should validate_presence_of :read_me}
	end
	describe "download" do
		it {should validate_presence_of :download}
	end

	it {should belong_to(:user)}
	it {should have_many(:template_group_pairs)}
	it {should have_many(:group_likes)}

end
