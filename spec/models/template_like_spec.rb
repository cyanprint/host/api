require 'rails_helper'

describe TemplateLike, type: :model do
	describe 'likes' do
		it 'should check for true or false' do
			expect(subject).to validate_inclusion_of(:like).in_array([true, false])
		end
	end

	it {should belong_to(:user)}
	it {should belong_to(:template)}
end
