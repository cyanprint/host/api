require 'rails_helper'

describe TemplateGroupPair, type: :model do
	it {should belong_to :template}
	it {should belong_to :group}

end
