require 'rails_helper'

describe Token, type: :model do
	describe 'secret' do
		it {should validate_uniqueness_of :secret}
		it {should validate_presence_of :secret}
		it {should allow_values("75159231d89bcf6c56ff935bdecb9b9f54e08703e622ec3f2f0ad85c7b93ad46",
								"5fd924625f6ab16a19cc9807c7c506ae1813490e4ba675f843d5a10e0baacdb8",
								"2413fb3709b05939f04cf2e92f7d0897fc2596f9ad0b8a9ea855c7bfebaae892").for(:secret)}
		it {should_not allow_values("asdfa", "2413fb3709b05939f04cf2e92f7d0897fc2596f9ad0b8a9ea855c7bfegaae892",
									"2413fb3709b05939f04cf2e92f7d0897fc2596f9ad0b8a9ea855c7bfebaae89",
									"2413fb3709b05939f04cf2e92f7d0897fc2596f9ad0b8a9ea855c7bfebaae8922").for(:secret)}
	end
	describe 'key' do
		it {should validate_presence_of :key}
		it {should allow_values("a", "azurekey", "Azure_key", "Azure-key").for(:key)}
		it {should_not allow_values("1", "azure*key", "azurekey!").for(:key)}
	end

	describe 'expiry' do
		it {should validate_presence_of :expiry}
	end

	it {should belong_to(:user)}


end
