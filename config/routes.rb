Rails.application.routes.draw do

	get '/template', to: 'public#templates'
	get '/template/:key', to: 'public#template_by_key'
	put '/template/:key', to: 'public#create_template'

	put '/template/:key/like', to: 'private#like_template'
	get '/template/:key/like', to: 'private#is_template_liked'


	get '/group', to: 'public#groups'
	get '/group/:key', to: 'public#group_by_key'
	put '/group/:key', to: 'public#create_group'

	put '/group/:key/like', to: 'private#like_group'
	get '/group/:key/like', to: 'private#is_group_liked'

	get '/token', to: 'private#tokens'
	post '/token', to: 'private#create_token'
	delete '/token/:id', to: 'private#delete_token'


	# put '/user', to: 'public#create_user'
	get '/user', to: 'public#users'
	get '/user/template', to: 'private#templates'
	get '/user/template/like', to: 'private#liked_templates'
	get '/user/group', to: 'private#groups'
	get '/user/group/like', to: 'private#liked_groups'

end
