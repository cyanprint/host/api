#!/bin/sh
set -e

rails db:create || :
rails db:migrate
echo "Completed database actions"
exec "$@"